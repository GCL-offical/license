# GCL - Generic Community License #

**Generic Community License**, GCL, is a license suits for all developers, and project owners. Project Owners can get profit by sharing some codes, and Developers can find some easy way to start developing.

## Why GCL is special? ##

Unlike other licenses, the best part of this license is you can actually make some money by sharing some of your Projects, Codes, and etc.

This license currently have 3 versions up:

* GCL-v1 - A default license, and a way to get easily set up. For people who use the code for commercial use, if the user having the copy earns more than 2,000$ by a software using codes with GCL license, the user with the copy has to lend 3% of the profit.
* GCL-v2 - A license for developers who wants to have more money, for people who think the project is something important for you. The lending amount is slightly increased, from 3% to 6%.
* GCL-v3 - A modified version of GCL-v1, which every user using the copy of the software commercially, no matter how much they earn, they must lend 3% of the profit earnt by software using codes with GCL license.

## Currently in progress! Please stay tuned.. ##